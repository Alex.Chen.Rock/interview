-- 老闆想知道公司裡，姓氏是BROWN的以外的員工有哪些，請列出他們的 no, age, firstname, lastname, salary，並依 no 由小到大排序
-- select no, age, firstname, lastname, salary from interview.staff where lastname != 'BROWN' order by no desc;

-- 老闆想知道公司裡，薪水在10000以下的員工加薪，請列出他們的加薪前薪水, 姓氏, 名字, 年齡, 加薪後薪水, 並依年齡排序由大到小排序
-- select salary, lastname, firstname, age, salary+10000 as adj_salary  from interview.staff where salary <= 10000 order by age;

-- 老闆想知道公司裡，我國員工有哪些(身分證符合格式的)，請列出他們的所有資料
-- select * from interview.staff where ID REGEXP '^[A-Z]{1}[1-2]{1}[0-9]{8}$';



SELECT Department, 
	   Employee,
       Weight
FROM
(SELECT
	e.id,
    d.name AS Department, 
    e.name AS Employee,
    e.weight AS Weight,
    SUM(weight) OVER (ORDER BY id) as totalWeight
FROM
    Employee e
JOIN
    Department d
ON
    e.departmentId = d.id
ORDER BY
    e.id) rid
WHERE
    totalWeight <= 300
ORDER BY
    totalWeight desc
 