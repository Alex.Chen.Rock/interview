# Data Engineer interview test

# 題目
/question/interview.ipynb

# 解答
/solution/solution.ipynb

# 應試者作答情況
/test_result

## 上機考試分數
總分 (滿分100 及格75)	  
---
Q1.python流程控制 (20)	  
Q2.python檔案處理 (20)	  
Q3. python 資料結構 (20)	  
Q4-1. SQL rank condition (20)	  
Q4-2. SQL join (20)  

https://docs.google.com/spreadsheets/d/1AF5fCM33_TUwzH4Ob58IigZsj74o_12uJF2rxJbPK_8/edit#gid=0